## Countries

This is a demo app

Containerized infrastructure provided by me at [AndersonPEM / cpmr-stack · GitLab](https://gitlab.com/andersonpem/cpmr-stack) (Yes, I do know some DevOps =D ) 



## How to run

All batteries are included in the env file, for clone and run. In a production environment, only the default values should be in the .env commited to the repository.

You must have Docker installed in your machine.

- Clone this repository

- On the working directory of the repository, type:
  
  ```bash
  docker-compose up -d
  ```

- Wait for the images to be pulled and the containers to be up

- Install the composer packages (mirroring your user permissions to the container) with:
  
  ```bash
  docker compose exec --user=docker php-fpm composer install
  ```

- In some machines, instead of **docker compose** you might need to use the **docker-compose** command.

- The app is served on http://localhost:8080

 

## Example of a request

```bash
curl -X GET "http://localhost:8080/routing/CZE/ITA"
```

## 

## Technical aspects

- This application uses Symfony 6.3 (Latest) at PHP 8.2. 

- Some aspects of the PHP 8.x are leveraged in this code, like the [Constructor property promotion](https://stitcher.io/blog/constructor-promotion-in-php-8)

- The routing algorithm implements a BFS (Breadth First Search) approach, exploring nodes in order of their distance. This approach is efficient for this kind of search.

- For maximum efficiency, the endpoint used to ingest data is only acessed once. When the data is retrieved through the http request, the data is immediately stored in a Redis memory database, providing fast access and response and reducing dependency on external sources. This cache has an expiration set up for it.

- Once a specific routing is requested, like the example above (CZE/ITA), the result of this routing is also cached to redis. This way, in the next request served, way less computational requirement  is needed to serve the same answer until the cache expires.
  
  

## Test coverage

Tests were wrote in **PHPUnit**. The coverage of the feature is as follows:

**tests/functional/RouteServiceTest**

- Assert if a valid route returns the proper value, with code 200

- Assert if a path with no land connection returns code 400 (Bad Request) with the message containing 'No land route found'

- Assert if bogus data (Invalid country code) is inserted, the API returns a 406 code (Not acceptable) and a message containing 'You have requested invalid data'



You can run the tests yourself by typing:

```bash
docker compose exec --user=docker php-fpm composer test
# or docker-compose if that's the case
```

The tests are executed in the pipeline automatically [Pipelines · AndersonPEM / Countries · GitLab](https://gitlab.com/andersonpem/countries/-/pipelines)

## Linting and Code style

This code is compliant with PSR-2 and PSR-12 coding standards.

You can verify compliance by running

```bash
docker compose exec --user=docker php-fpm vendor/bin/phpcs
# or docker-compose if that's the case
```



The coding style and unit tests are executed in the pipeline for the project:
[Pipelines · AndersonPEM / Countries · GitLab](https://gitlab.com/andersonpem/countries/-/pipelines)



## API documentation

I deemed API docs not to be necessary, since it uses a single endpoint with a single verb, and this API is not supposed to go in production. The lack of complexity unjustifies the need for it.
