<?php

namespace App\Tests\Functional;

use App\Service\RouteService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * For this test suite the following conventions were established:
 * - Specify routes directly instead of fetching named routes.
 *
 */
class RouteServiceTest extends WebTestCase
{
    private \Symfony\Bundle\FrameworkBundle\KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * Asserts a known path
     *
     * @test
     * @return void
     */
    public function testValidRoute(): void
    {
        // Perform the GET request
        $this->client->request('GET', '/routing/CZE/ITA');

        // Assert that the response status code is 200 (OK)
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        // Decode the response content
        $responseContent = json_decode($this->client->getResponse()->getContent(), true);

        // Assert that the route is as expected
        $this->assertEquals(['CZE', 'AUT', 'ITA'], $responseContent['route']);
    }

    /**
     * Tests if a route with no land possibility fails
     *
     * @test
     * @return void
     */
    public function testInvalidRoute(): void
    {

        // Perform the GET request for an invalid route
        $this->client->request('GET', '/routing/CZE/GBR');

        // Assert that the response status code is 400 (Bad Request)
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        // Decode the response content
        $responseContent = json_decode($this->client->getResponse()->getContent(), true);

        // Assert that the error message is as expected
        $this->assertEquals('No land route found', $responseContent['error']);
    }

    /**
     * Tests if I insert random data which is not a country code, the API fails gracefully.
     *
     * @test
     * @return void
     */
    public function testBadUserData(): void
    {

        // Perform the GET request for an invalid route
        $this->client->request('GET', '/routing/XXX/VVV');

        // Assert that the response status code is 400 (Bad Request)
        $this->assertEquals(406, $this->client->getResponse()->getStatusCode());

        // Decode the response content
        $responseContent = json_decode($this->client->getResponse()->getContent(), true);

        // Assert that the error message is as expected
        $this->assertEquals('You have requested invalid data', $responseContent['error']);
    }
}
