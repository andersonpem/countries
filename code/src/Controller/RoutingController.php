<?php

namespace App\Controller;

use App\Service\CountryService;
use App\Service\RouteService;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class RoutingController extends AbstractController
{
    /**
     * Again, using PHP 8's promoted attributes to minimize code writing.
     *
     * @param RouteService $routeService
     */
    public function __construct(private readonly RouteService $routeService)
    {
    }

    /**
     * Possible improvements:
     * - Add validators outside the controller
     *
     * @param $origin
     * @param $destination
     * @return JsonResponse
     * @throws InvalidArgumentException|\Psr\Cache\InvalidArgumentException
     */
    #[Route('/routing/{origin}/{destination}', name: 'app_routing', methods: 'GET')]
    public function routing($origin, $destination): JsonResponse
    {
        $route = $this->routeService->findRoute($origin, $destination);


        if ($route) {
            return new JsonResponse(['route' => $route]);
        }

        /**
         * Null indicates the data is valid, however, no match was found
         */
        if (null === $route) {
            return new JsonResponse(['error' => 'No land route found'], 400);
        }

        /**
         * The other only possible state of the machine at this point is false.
         * In this case, return a "Not acceptable" to the user
         */
        return new JsonResponse(['error' => 'You have requested invalid data'], 406);
    }
    #[Route('/')]
    public function index(): JsonResponse
    {
        return new JsonResponse(['message' => 'I am a teapot.'], 418);
    }
}
