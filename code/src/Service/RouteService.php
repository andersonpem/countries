<?php

namespace App\Service;

use Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class RouteService
{
    private CacheInterface $cache;
    private CountryService $countryService;
    private array $countries;

    public function __construct(
        CacheInterface $cache,
        CountryService $countryService,
        array $countries = []
    ) {
        $this->cache = $cache;
        $this->countryService = $countryService;
        $this->countries = $countries ?: $this->countryService->getCountries();
    }

    /**
     * This function implements a BFS (Breadth First Search) algorithm.
     * It explores nodes in order of their distance from the source node.
     *
     * @param string $origin
     * @param string $destination
     * @return array
     * @throws InvalidArgumentException
     */
    public function findRoute(string $origin, string $destination)
    {
        $cacheKey = "route_{$origin}_{$destination}";

        // Try to fetch the route from cache
        return $this->cache->get($cacheKey, function (ItemInterface $item) use ($origin, $destination) {
            $item->expiresAfter(3600);  // Cache for 1 hour

            $visited = [];
            $queue = [];
            $prev = [];

            $queue[] = $origin;

            while (!empty($queue)) {
                $country = array_shift($queue);
                $visited[$country] = true;

                if ($country === $destination) {
                    $path = [];
                    while ($country) {
                        array_unshift($path, $country);
                        $country = $prev[$country] ?? null;
                    }
                    return $path;
                }
                try {
                    foreach ($this->countries[$country]['borders'] as $neighbor) {
                        if (!isset($visited[$neighbor])) {
                            $visited[$neighbor] = true;
                            $queue[]            = $neighbor;
                            $prev[$neighbor]    = $country;
                        }
                    }
                // Invalid data was sent by the user
                } catch (Exception $e) {
                    return false;
                }
            }

            return null;
        });
    }
}
