<?php

namespace App\Service;

use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CountryService
{
    private const COUNTRIES_CACHE_KEY = 'indexed_countries';

    public function __construct(private HttpClientInterface $client, private CacheInterface $cache)
    {
    }

    public function getCountries(): array
    {
        // Try to fetch the indexed countries from cache
        return $this->cache->get(self::COUNTRIES_CACHE_KEY, function (ItemInterface $item) {
            // If not in cache, fetch the raw countries data
            $rawCountriesData = $this->fetchRawCountriesData();

            // Index the countries data by the 'cca3' field
            $indexedCountries = [];
            foreach ($rawCountriesData as $countryData) {
                $indexedCountries[$countryData['cca3']] = $countryData;
            }

            // Set the cache to expire after 1 day (or any desired duration)
            $item->expiresAfter(86400);  // 86400 seconds = 1 day

            return $indexedCountries;
        });
    }

    private function fetchRawCountriesData(): array
    {
        return json_decode($this->client->request(
            'GET',
            'https://raw.githubusercontent.com/mledoze/countries/master/countries.json'
        )
            ->getContent(), true);
    }
}
